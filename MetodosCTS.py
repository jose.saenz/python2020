# frase = "Aprendemos a programar en python"
# course[0]   # Devuelve el primer caracter
# course[1]   # Devuelve el segundo caracter
# course[-1]   # Devuelve el primer caracter iniciando por el final
# course[-2]   # Devuelve el segundo caracter iniciando por el final

# substring

# frase = "Aprendemos a programar en python"
# frase[1:5]
# print()

frase = "Aprendemos a programar en python"
frase[:5]
