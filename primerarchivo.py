# print("Hola Mundo")
# print("Nueva linea")

"""
Esto es un comentario
Siempre sobre el mismo comentario
"""
x = 5

if x == 5:
    print("Primer Mensaje")  # Esto es un comentario
    print("Variable tiene valor de 5")  # Segundo comentario
    print(type(x))


x = "Python"
print(x)
print(type(x))

x = 5.0
print(type(x))

x = True
print(type(x))